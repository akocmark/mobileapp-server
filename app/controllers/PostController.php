<?php

class PostController extends BaseController {
	private $page 			= 1;
	private $itemPerPage 	= 8;

	public function index() {
		if (Input::get('page')) {
			$this->page = (int) Input::get('page');
		}

	    $posts = Post::skip(($this->page - 1) * $this->itemPerPage)
	    	->take($this->itemPerPage)
	    	->with('user')
	    	->get();

	    return Response::make(json_encode(array(
	    	'error' => false,
	    	'posts' => $posts
	    )));
	}

	public function show($id) {
		$post = Post::where('id', '=', $id)->with('user')->first();

		return Response::json(array(
			'error' => false,
			'post' 	=> $post,
		));
	}

	public function destroy($id) {
		Post::find($id)->delete();

		return Response::json(array(
			'error' => false,
		));
	}

}