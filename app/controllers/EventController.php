<?php

class EventController extends BaseController {
	public function store() {
		$event = new CalendarEvent;
		$event->name 		= Input::get('name');
		$event->description = Input::get('desc');
		$event->date 		= Input::get('date');
		$event->save();

		return Response::json(array(
			'error' => false,
		));
	}

	public function getEventsByMonth($month) {
		$startDate 	= mktime(0, 0, 0, $month, 1, date("y"));
		$endDate 	= mktime(0, 0, 0, $month, date('t'), date("y"));

		$events = CalendarEvent::whereBetween('date', array(
			date('Y-m-d', $startDate), date('Y-m-d', $endDate))
		)->get();

		if (!empty($events)) {
			$new_events = array();
			foreach ($events as $key => $value) {
				$new_events[date('j', strtotime($value->date))] = $value;
			}
			$events = $new_events;
		}
		return Response::json(array(
			'error' 	=> false,
			'events' 	=> $events,
		));
	}
}
