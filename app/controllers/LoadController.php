<?php

class LoadController extends Controller {

	public function redeem($code) {
		$load = LoadCard::where('code', '=', $code)
			->where('status', '=', 'active')
			->first();

		if (empty($load)) {
			return Response::json(array(
				'error' => true,
				'message' => 'Sorry your code is invalid.',
			));			
		}

		$loadValue = (int) $load->value;

		$user = User::find(Input::get('_id'));
		$user->load = $user->load + $loadValue;
		$user->save();

		$load->value = 0;
		$load->status= 'redeemed';
		$load->save();

		return Response::json(array(
			'error' => false,
			'data' => $load
		));
	}

}
