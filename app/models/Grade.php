<?php

class Grade extends Eloquent {
	public function gradingRule() {
		return $this->belongsTo('GradingRule');
	}
}
