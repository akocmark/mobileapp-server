<?php

class SubjectController extends BaseController {

	public function show($id) {
		$subject = Subject::where('id', '=', $id)->with('user')->first();

		$posts = Post::where('subject_id', '=', $id)->with('user')->orderBy('updated_at', 'desc')->get();

		return Response::json(array(
			'error' 	=> false,
			'subject' 	=> $subject,
			'posts' 	=> $posts,
		));
	}

	public function store() {
		$subject 				= new Subject;
		$subject->code 			= Input::get('code');
		$subject->name 			= Input::get('name');
		$subject->schedule 		= Input::get('schedule');
		$subject->description 	= Input::get('description');
		$subject->user_id 		= Input::get('user_id');
		$subject->save();

	    return Response::make(json_encode(array(
	    	'error' => false,
	    )));
	}

	public function addPost($subjectId) {
		$post = new Post;
		$post->body = Input::get('post');
		$post->user_id = Input::get('_id');
		$post->subject_id = Input::get('subject_id');
		$post->save();

	    return Response::make(json_encode(array(
	    	'error' => false,
	    	'post' 	=> $post,
	    )));
	}

	public function info($id) {
		$subject = Subject::where('id', '=', $id)->with('user')->first();

	    return Response::json(array(
	    	'error' 	=> false,
	    	'subject' 	=> $subject
	    ));
	}

	public function students($id) {
		$students = Subject::find($id)->users;

	    return Response::json(array(
	    	'error' 	=> false,
	    	'students' 	=> $students
	    ));
	}

	public function addStudent($subject_id) {
		$subject = Subject::find($subject_id);
		$subject->users()->attach(Input::get('user_id'));

	    return Response::json(array(
	    	'error' => false,
	    ));	
	}

	public function gradingRule($id) {
		$rules = Subject::find($id)->gradingRules->toJson();
		$rules = json_decode($rules);

		if (empty($rules) && Input::get('allowEmpty') != 'true') {
			return Response::json(array(
				'error' 	=> true,
				'message' 	=> 'Please set up grading rule first before adding grades.',
				'empty'  	=> true,
			));
		}

		return Response::json(array(
			'error' => false,
			'rules' => $rules
		));
	}

	public function addGradingRule($id) {
		$rule 				= new GradingRule;
		$rule->subject_id 	= $id;
		$rule->criteria 	= Input::get('criteria');
		$rule->percent 		= Input::get('percent');
		$rule->save();

		return Response::json(array(
			'error' => false,
		));
	}
}