<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

// api login
Route::post('api/login', 'UserController@login');
Route::post('api/register', 'UserController@register');

// API Version 1
Route::group(array('prefix' => 'api/v1', 'before' => 'api.auth'), function()
{
    Route::get('users/userload', 'UserController@userLoad');
    Route::get('users/signups', 'UserController@signups');
    Route::get('users/{id}/subjects', 'UserController@subjects');
    Route::get('users/search/{query}', 'UserController@search');
    Route::get('users/{id}/activate', 'UserController@activate');
    Route::get('users/{id}/stories', 'UserController@stories');
    Route::get('users/{id}/grades', 'UserController@grades');
    Route::post('users/{id}/savegrades', 'UserController@saveGrades');
    Route::resource('users', 'UserController');

    Route::resource('posts', 'PostController');

    Route::post('subjects/{id}/posts', 'SubjectController@addPost');
    Route::get('subjects/{id}/info', 'SubjectController@info');
    Route::get('subjects/{id}/students', 'SubjectController@students');
    Route::post('subjects/{id}/students', 'SubjectController@addStudent');
    Route::get('subjects/{id}/gradingrule', 'SubjectController@gradingRule');
    Route::post('subjects/{id}/gradingrule', 'SubjectController@addGradingRule');
    Route::resource('subjects', 'SubjectController');

    Route::get('events/month/{month}', 'EventController@getEventsByMonth');
    Route::resource('events', 'EventController');

    Route::resource('news', 'NewsController');

    Route::get('load/{code}/redeem', 'LoadController@redeem');
});