<?php

class UserController extends BaseController {

	public function index() {
	    $users = User::with(array('role' => function($query) {
	    	$query->where('id', '=', 1);
	    }))->first();

	    return Response::make(json_encode(array(
	    	'error' => false,
	    	'users' => $users
	    )));
	}

	public function login() {
		$user = User::where('email', '=', Input::get('email'))->with('role')->first();

		if (!$user) {
			return Response::json(array(
				'error' 	=> true,
				'message' 	=> 'User not found.',
			));
		}

		if (!Hash::check(Input::get('password'), $user->password)) {
			return Response::json(array(
				'error' 	=> true,
				'message' 	=> 'Password is incorrect.',
			));
		}

		if ($user->status == 'inactive') {
			return Response::json(array(
				'error' 	=> true,
				'message' 	=> 'Account inactive. Please wait for your account to be verified by a staff.',
			));
		}

		$user->session_id = md5($user->email + time());
		$user->save();

		$session_user = $user;

		return Response::json(array(
			'error' 	=> false,
			'user' 		=> $session_user,
			'status' 	=> 'connected',
		));
	}

	public function register() {
		$user = User::where('email', '=', Input::get('email'))->first();
		if (!empty($user)) {
			return Response::json(array(
				'error' => true,
				'message' 	=> 'Email is already taken.',
			));
		}
		
		$user = new User;

		$user->role_id 		= 3;
		$user->name 		= Input::get('name');
		$user->username 	= Input::get('email');
		$user->email 		= Input::get('email');
		$user->password 	= Hash::make(Input::get('password'));
		$user->course 		= Input::get('course');
		$user->yearsection 	= Input::get('yearsection');
		$user->status 		= 'inactive';

		$user->save();

		return Response::json(array(
			'error' 	=> false,
		));
	}

	public function subjects($id) {
		$user = User::find($id);
		$role = $user->role;

		if ($role->name == 'prof') {
	    	$subjects = Subject::where('user_id', '=', $id)->get();
		} elseif ($role->name == 'student') {
	    	$subjects = $user->subjects;
		}


	    return Response::json(array(
	    	'error' 	=> false,
	    	'subjects' 	=> $subjects
	    ));
	}

	public function search($query) {
		$users = User::where('name', 'LIKE', '%'.$query.'%')
			->where('status', '=', 'active')
			->with('role')
			->with('subjects')
			->get();

		foreach ($users as $key => $value) {
			if ($value->role->name !== 'student') {
				unset($users[$key]);
				continue;
			}

			foreach ($value->subjects as $k => $v) {
				if ($v->id == 21) {
					unset($users[$key]);
				}
			}
		}

	    return Response::json(array(
	    	'error' 	=> false,
	    	'students' 	=> $users
	    ));	
	}

	public function signups() {
		$users = User::where('status', '=', 'inactive')->get();

	    return Response::json(array(
	    	'error' 	=> false,
	    	'students' 	=> $users
	    ));		
	}

	public function activate($id) {
		$user = User::find($id);

		if ($user->status == 'active') {
		    return Response::json(array(
		    	'error' 	=> true,
		    	'message' 	=> 'User account was already activated.',
		    ));	
		}

		$user->status = 'active';
		$user->save();

	    return Response::json(array(
	    	'error' 	=> false,
	    ));	
	}

	public function stories($id) {
		$user 		= User::find($id);
		$stories 	= array();
		$subjects 	= array();
		$posts 		= array();
		

		if ($user->role->name == 'prof') {
			$subjects = Subject::where('user_id', '=', $user->id)->get();

			foreach ($subjects as $key => $value) {
				$posts[$value->id] = Post::where('subject_id', '=', $value->id)
					->with('user')
					->take(3)
					->orderBy('updated_at', 'desc')
					->get();
			}

		} elseif ($user->role->name == 'student') {
			$subjects = $user->subjects;

			foreach ($subjects as $key => $value) {
				$posts[$value->id] = Post::where('subject_id', '=', $value->id)
					->with('user')
					->take(3)
					->orderBy('updated_at', 'desc')
					->get();
			}
		}

		foreach ($subjects as $key => $value) {
			$stories[$key] 			= $value;
			$stories[$key]['posts'] = $posts[$value->id];
		}

	    return Response::json(array(
	    	'error' 	=> false,
	    	'stories' 	=> $stories,
	    ));		
	}

	public function userLoad() {
		if (!Input::has('_id')) {
			return Response::json(array(
				'error' 	=> true,
				'message' 	=> 'Unable to get load balance.',
			));
		}

		$userLoad = User::select('load')->where('id', '=', Input::get('_id'))->first();

		return Response::json(array(
			'error' => false,
			'load'  => $userLoad->load,
		));		
	}

	public function grades($id) {
		$user = User::find($id);

		$grades = $user->grades;

		$temp = array();
		foreach ($grades as $key => $value) {
			$temp[$value->grading_rule_id] = $value;
		}
		$grades = $temp;

		return Response::json(array(
			'error' => false,
			'grades'  => $grades,
			'user' => $user,
		));		
	}

	public function saveGrades($id) {
		foreach (Input::get('grades') as $key => $value) {
			$grade = Grade::where('grading_rule_id', '=', (int) $key)
				->where('subject_id', '=', (int) Input::get('subject_id'))
				->where('user_id', '=', (int) Input::get('user_id'))
				->first();

			if (!$grade) {
				$grade = new Grade;
			}

			$grade->user_id 	 	= (int) Input::get('user_id');
			$grade->subject_id 	 	= (int) Input::get('subject_id');
			$grade->grading_rule_id = (int) $key;
			$grade->value 		 	= (int) $value;
			$grade->save();
		}

		return Response::json(array(
			'error' => false,
		));	
	}
}