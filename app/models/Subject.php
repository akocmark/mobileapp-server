<?php

class Subject extends Eloquent {

	public function user() {
        return $this->belongsTo('User');
	}

	public function users() {
        return $this->belongsToMany('User');
	}

	public function posts() {
		return $this->hasMany('Post');
	}

	public function gradingRules() {
		return $this->hasMany('GradingRule');
	}
}
