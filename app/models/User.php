<?php

class User extends Eloquent {
	public function role() {
        return $this->belongsTo('Role');
	}

	public function posts() {
        return $this->hasMany('Post');
	}

	public function subjects() {
        return $this->belongsToMany('Subject');	
	}

	public function grades() {
		return $this->hasMany('Grade');
	}
}
