<?php

class NewsController extends Controller {
	private $page 			= 1;
	private $itemPerPage 	= 8;

	public function index() {
		if (Input::get('page')) {
			$this->page = (int) Input::get('page');
		}

	    $news = News::skip(($this->page - 1) * $this->itemPerPage)
	    	->take($this->itemPerPage)
	    	->with('user')
	    	->orderBy('updated_at', 'desc')
	    	->get();

	    return Response::json(array(
	    	'error' => false,
	    	'news' => $news
	    ));
	}

	public function show($id) {
		$news = News::where('id', '=', $id)->with('user')->first();

		return Response::json(array(
			'error' => false,
			'news' 	=> $news,
		));
	}

	public function store() {
		$news 			= new News;
		$news->title 	= Input::get('title');
		$news->body 	= Input::get('body');
		$news->user_id 	= Input::get('_id');
		$news->save();

		$user = User::find(Input::get('_id'));
		if ($user->role->name != 'admin' ) {
			$user->load = $user->load - 1;
			$user->save();
		}

		return Response::json(array(
			'error' => false
		));
	}

	public function destroy($id) {
		News::find($id)->delete();

		return Response::json(array(
			'error' => false,
		));
	}

}
